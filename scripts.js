// Define object containing your Raphael path data.
// goto http://lazylinepainter.info to convert your svg into a svgData object.


//preLogo svg paint
var svgData = {
  "preLogo" :
    {
      "strokepath" :
        [
          {
            "path": "M32.922.31L16.529 7.358v-.02l-.02.011-.022-.01v.02L.095.31v29.333h10.808v-2.221l5.584 2.219.023-.01.02.01 5.584-2.22v2.222h10.808z",
            "duration": 300
          },

          {
            "path": "M34.382 29.644h34.75L51.756.311 34.382 29.644zm17.374-9.898l1.756 3.358H50l1.756-3.358z",
            "duration": 300
          },
          {
            "path": "M103.419.31L87.026 7.358v-.02l-.02.011-.023-.01v.02L70.591.31v29.333H81.4v-2.221l5.583 2.219.023-.01.02.01 5.584-2.22v2.222H103.42z",
            "duration": 300
          },
          {
            "path": "M104.877 29.644h34.752L122.252.311l-17.375 29.333zm17.376-9.898l1.756 3.358h-3.512l1.756-3.358z",
            "duration": 300
          },
          {
            "path": "M0 51.887v-9.186h3.842c.495 0 .939.068 1.335.204.395.134.713.304.957.511.243.205.444.448.605.73.162.279.273.552.333.82.06.266.09.535.09.805 0 .35-.054.695-.162 1.031a3.363 3.363 0 0 1-.521.984c-.239.32-.586.577-1.041.774-.456.197-.989.296-1.596.296H2.553v3.031H0zm2.553-7.166v2.112h.729c.833 0 1.25-.352 1.25-1.062 0-.699-.417-1.05-1.25-1.05h-.729zM33.755 51.888h2.553V42.7h-2.553zM63.327 42.7h7.384l-4.102 6.981h3.737v2.206h-7.748l4.194-6.982h-3.465zM97.098 42.7h7.384l-4.102 6.981h3.737v2.206h-7.748l4.194-6.982h-3.465zM130.137 51.887l3.243-9.186h3.099l3.243 9.186h-2.735l-.508-1.59h-3.099l-.508 1.59h-2.735zm4.793-6.994l-1.043 3.386h2.084l-1.041-3.386z",
            "duration": 700
          }
        ],
      "dimensions": {
        "width": 140,
        "height": 52
      }
    }
}
//preLogo svg paint END

$.fn.itemheight = function(){
        var $blocks = $(this);
        var maxH = $(this).height();
        $($blocks).each(function(){
            if($(this).height() > maxH){
                maxH = $(this).height();
            }
        });

        $blocks.height(maxH);
    }

$('.cat-product-one').itemheight();


//FUNCTIONS WITHOUT JQUERY

//function hex to rgb
function hexToRgbNew(hex) {
  var arrBuff = new ArrayBuffer(4);
  var vw = new DataView(arrBuff);
  vw.setUint32(0,parseInt(hex, 16),false);
  var arrByte = new Uint8Array(arrBuff);
  return arrByte[1] + "," + arrByte[2] + "," + arrByte[3];
}
//function hex to rgb

function MyRating(){
      $('.order-raty').raty();
}

$(document).ready(function(){

	var lgScreen = 1300,
		mdScreen = 1024,
		smScreen = 768;


  


	$('.js-phone-mask').mask('?+7 (999) 999-9999', { placeholder: '_' });


  $('body').addClass('is-loading');

  //FUNCTIONS WITH JQUERY

  //preLogo svg animate
  $('#preLogo').lazylinepainter({
    "svgData": svgData,
    "strokeWidth": 1,
    'onStart' : function(){
      $('#preLogo path:nth-of-type(1)').addClass('first');
      $('#preLogo path:nth-of-type(2)').addClass('second');
      $('#preLogo path:nth-of-type(3)').addClass('third');
      $('#preLogo path:nth-of-type(4)').addClass('fourth');
      $('#preLogo path:nth-of-type(5)').addClass('fifth');
    },
    'onComplete' : function(){
      $('#preLogo').addClass('preLogo-ready');
      function func() {
        $('#preLogo').addClass('preLogo-readyNext');
      }
      setTimeout(func, 280);
    }
  })
  $('#preLogo').lazylinepainter('paint');
  //preLogo svg animate END


  //function hight == highest element
  function max_height(block, height_block){
    var mh = 0;
    block.each(function () {
      var h_block = parseInt($(this).height());
      if(h_block > mh) {
        mh = h_block;
      };
    });
    height_block.height(mh);
  }
  //function height == highest element END

  //function child height == parent height
  function parent_height(child, parent){
    var child_height = child.height();
    parent.height(child_height);
  }
  //function child height == parent height END

  //function scrollWidth
  $.scrollbarWidth = function() {
    var parent, child, width;

    if(width===undefined) {
      parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');
      child=parent.children();
      width=child.innerWidth()-child.height(99).innerWidth();
      parent.remove();
    }

    return width;
  };
  //function scrollWidth END

  //FUNCTIONS WITH JQUERY END

  //main slider
  $('.owl-carousel.main-slider').owlCarousel({
    loop:true,
    nav:false,
    items: 1,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true
  });
  //main slider END

  $(document).on('click', '[data-openparent]', function(){
    var open_elem = $(this).data('openparent');
    $('[data-openChild="' + open_elem + '"]').slideToggle();
    $(this).toggleClass('active');
  })

  $('.cat-product-box').matchHeight();
	$('.news-pret-box').matchHeight();

  //modal right
  function showModal($t) {
    let first = $('.container-fluid:first-child');
    let width = $t.children('.modal-inner').width();
    let mRight = $t.offset().left <= 0 ? 0 : $.scrollbarWidth();
    let offset_right = $(window).width() - first.offset().left - first.outerWidth();

    $t
      .css({ width: 0, opacity: 1, display: 'block', marginRight: -mRight, right: offset_right })
      .animate({ width: width }, 400)
	  .addClass('show');
    $('body')
      .css({ 'padding-right': $.scrollbarWidth() })
      .append('<div class="modal-opacity"></div>')
      .addClass('modal-open modal-open-right');
    $('.modal-opacity').fadeIn(400);
  }
  $(document).on('click', '[data-toggle="modal-right"]', function() {
    let target = $(this).data('target'), $t = $(target);

    if ($t.length > 0) {
      showModal($t);
    } else {
      sendAjax(this, { ACTION: target.substr(1).toUpperCase(), MODULE: 'MODAL' })
        .then(function(data) {
          let $t = $(data);
          $('body').append($t.show());
          $t.children('.modal-inner').width($t.width());

          $t.find('.js-phone-mask').mask('?+7 (999) 999-9999', { placeholder: '_' });

          let $tabs = $t.find('.tab-box');
          if ($tabs.length) {
            $tabs.each(function() {
              let $t = $(this);
              $t.find('.tab-btn-one').each(function(i, val) {
                $(this).attr('data-tabbtn', i);
              });
              $t.find('.tab-content-one').each(function(i, val) {
                $(this).attr('data-tabcontent', i);
              });
              let $active = $t.find('.tab-content-one.active');
              tab($t, $active.length > 0 ? $active.data('tabbtn') : 1);
            });
          }

          showModal($t.hide());
        })
      ;
    }
    return false;
  });
  $(document).on('click', '[data-dismiss="modal-right"]', function() {
    var $t = $(this).parents('.modal-right');
    var width = $t.width();
    $t
      .animate({ width: 0, opacity: 1 }, 400, function() {
        $(this).hide().css('width', width);
      })
	    .removeClass('show');
    $('body')
      .css({'padding-right': ''})
      .removeClass('modal-open modal-open-right');
    $('.modal-opacity').fadeOut(400, function() {
      $('.modal-opacity').remove();
    });
    location.hash = '#none';
  });
  //modal right END


  //modal left




    $(document).on('click', '[data-toggle="modal-left"]', function(){

    if($('body').hasClass('cart-open')){
      $('.m-basket-close').trigger('click');
    }



    if(!$(this).hasClass('active') && !($(this).data('dismiss'))){
      var to_open = $(this).data('target');


        var top_of_element = $(".attention-row").offset().top;
        var bottom_of_element = $(".attention-row").offset().top + $(".attention-row").outerHeight();
        var bottom_of_screen = $(window).scrollTop() + $(window).height();
        var top_of_screen = $(window).scrollTop();

        var extra_top = 0;
        if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){

          var margin_t = $('.attention-row').css('margin-top');
          margin_t = margin_t.match(/[\d\.]+/g);
          extra_top = inViewport($(".attention-row"));

        }

      $(to_open)
				.css({
					'left' : '-100%',
					'top' : $('.f-mobile-top').height() - 2 + extra_top
				})
        .animate({'left' : 0}, 200);

      $(this).addClass('active');

      if(typeof($(this).data('dismiss')) != undefined){
        $(this).attr('data-dismiss', 'modal-left');
      }


      $('body')
        .append('<div class="modal-opacity"></div>')
        .addClass('modal-open modal-open-left');
      $('.modal-opacity').fadeIn(400);
    }
  })

  $(document).on('click', '[data-dismiss="modal-left"]', function(){
    if($(this).data('toggle') == 'modal-left'){
      var close_element = $(this).data('target');
      $(close_element).animate({'left' : '-100%'}, 400);
      $('body')
        .removeClass('modal-open')
        .removeClass('modal-open-left');
      $('.modal-opacity').fadeOut(400, function(){
        $('.modal-opacity').remove();
      })
      $(this).removeClass('active');
      $(this).removeAttr('data-dismiss');
      location.hash = '#none';
    }
  })



  //modal left END



  //modal center
  function showModalCenter($t) {
    "use strict";

    var sto_top = $(window).height() - $t.find('.modal-center-in').height()
    sto_top = sto_top/2;
    if(sto_top < 20){
      sto_top = 20;
    }

    $t.addClass('active')
      .css({ top: sto_top, visibility: 'visible', opacity: '0' })
      .animate({ opacity: 1 }, 300)
    ;

    $('body')
      .css({ paddingRight: $.scrollbarWidth() })
      .append('<div class="modal-opacity"></div>')
      .addClass('modal-open modal-open-center');
    $('.modal-opacity').fadeIn(300);
  }
  $(document).on('click', '[data-toggle="modal-center"]', function(){
    showModalCenter($($(this).data('target')));
  });
  $(document).on('click', '[data-dismiss="modal-center"]', function(){
    $(this).parents('.modal-center')
      .removeClass('active')
      .animate({
        'opacity' : '0'
      }, 300, function(){
        $(this)
          .css({
            'top' : '',
            'visibility' : '',
            'opacity' : ''
          })
      })
    $('body')
      .removeClass('modal-open modal-open-center')
      .css({
        'padding-right' : ''
      })
    $('.modal-opacity').fadeOut(300, function(){
      $(this).remove();
    });
    location.hash = '#none';
  })
  //modal center END


function inViewport($el) {
    var elH = $el.outerHeight(),
        H   = $(window).height(),
        r   = $el[0].getBoundingClientRect(), t=r.top, b=r.bottom;
    return Math.max(0, t>0? Math.min(elH, H-t) : (b<H?b:H));
}


  //modal cart mobile

  $(document).on('click', '[data-btn="mobile-basket"]', function(){
    if($('body').hasClass('modal-open-left')){
      $('[data-dismiss="modal-left"]').trigger('click');
    }

    var win_height = $(window).height();
    var cart_height = $('.cart-left').height();

    $('.cart-left').wrap('<div class="cart-left-out"></div>');
    $('.cart-left-out').css('height', cart_height);
	
	var n_top = 0;
	
	var top_of_element = $(".attention-row").offset().top;
    var bottom_of_element = $(".attention-row").offset().top + $(".attention-row").outerHeight();
    var bottom_of_screen = $(window).scrollTop() + win_height;
    var top_of_screen = $(window).scrollTop();

	
	
    if((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
		n_top = inViewport($(".attention-row"));
    }

    $('body')
      .addClass('modal-open')
      .addClass('cart-open');
    $(this)
      .css({
        'position' : 'relative',
        'left' : '0px'
      })
      .animate({
        'left' : '100px'
      }, 200, function(){
        $(this).hide();
        $('.cart-left')
          .css({
            'position' : 'fixed',
            'left' : win_height,
            'top' : $('.f-mobile-top').height() - 2 + n_top,
            'bottom' : 0,
            'margin-left' : 0,
            'margin-right' : 0,
            'width' : '100%'
          })
          .animate({
            'left' : '0'
          }, function(){
            $(this).css({
              'overflow-y' : 'auto'
            })
          })
        $('.m-basket-close')
          .addClass('active')
          .animate({
            'left' : '0'
          })
      });
    $('.mobile-menu-btn').addClass('inactive');
  })

  $(document).on('click', '.m-basket-close', function(){
    $(this)
      .css({
        'left' : 0
      })
      .animate({
        'left' : '100px'
      }, function(){
        $(this).removeClass('active');
      })
    $('.cart-left')
      .css({
        'left' : 0
      })
      .animate({
        'left' : $(window).width()
      }, function(){
        $(this)
          .css({
            'position' : '',
            'left' : '',
            'top' : '',
            'margin-left' : '',
            'margin-right' : '',
            'width' : '',
            'overflow-y' : ''
          })
        $('.cart-left').unwrap();
        $('.mobile-menu-btn').removeClass('inactive');
        $('body')
          .removeClass('cart-open')
          .removeClass('modal-open');
        $('[data-btn="mobile-basket"]')
          .show()
          .css({
            'left' : '100px',
            'opacity' : 0
          })
          .animate({
            'left' : '0',
            'opacity' : 1
          })
      })
  })

  //modal cart mobile END


  //modal bottom

  $(document).on('click', '[data-toggle="modal-bottom"]', function(){
    var open_elem = $(this).data('target');
    $(open_elem)
      .css({
        "top":"100%",
        'display' : 'block'
      })
      .animate({
        'top' : 0
      }, 400);
    $('.f-mobile-top').removeClass('show');
		$('body').addClass('modal-open');
	})

	$(document).on('click', '[data-dissmiss="modal-bottom"]', function(){
		$(this).parents('.modal-bottom')
			.animate({
				'top': '100%'
			}, 400, function(){
				$(this)
					.css({
						'display': '',
						'top' : ''
					})
			});
		$('body').removeClass('modal-open');
  })

  //modal bottom END







  //modal opacity click to close modal
  $(document).on('click', '.modal-opacity', function(){
    if($('body').hasClass('modal-open-right')){
      $('[data-dismiss="modal-right"]').trigger('click');
    }
    if($('body').hasClass('modal-open-left')){
      $('[data-dismiss="modal-left"]').trigger('click');
    }

    if($('body').hasClass('modal-open-center')){
      $('[data-dismiss="modal-center"]').trigger('click');
    }
  })
  //modal opacity click to close modal END

  $(document).on('click', '[data-catalog]', function(){
    $('.cart-extra-catalog').removeClass('active');
    $(this).addClass('active');
    $('.cart-extra-slider').hide();
    $('.cart-extra-slider[data-catslider="' + $(this).data('catalog') + '"]').show();
    $('[data-catslider="' + $(this).data('catalog') + '"]').find('.owl-carousel.cart-extra-owl').find('.owl-item.active').eq(1).find('.cart-sl-title, .cart-sl-count').show();
    $('[data-catslider="' + $(this).data('catalog') + '"]').find('.owl-carousel.cart-extra-owl').find('.owl-item.active').eq(1).addClass('show_title');
  })

  $('.logo').clone().appendTo('.s-mobile-header');
  $('.s-col-info').clone().appendTo('.s-mobile-header');


  $('.meal-menu').clone().appendTo('.mobile-left-menu-in');
  $('.stock-box').clone().appendTo('.mobile-left-menu-in');
  $('.login').clone().appendTo('.mobile-left-menu-in');
  $('.promo-box').clone().appendTo('.mobile-header');
  $('.topr-menu').clone().appendTo('.mobile-left-menu-in');
  $('.about-menu-box').clone().appendTo('.mobile-left-menu-in');



  $(window).on('resize', function(){
		/* max_height($('.promo-img img'), $('.promo-box-in')); */
		/* max_height($('.promo-img img'), $('.promo-title')); */
    parent_height($('.t-header-in'), $('.t-header'));
    parent_height($('.f-mobile-top'), $('.f-mobile-header'));
    //width more than 768



    var window_width = $(window).width();

    window_width = parseInt(window_width);

		if(window_width >= smScreen) {

      $(window).scroll(function(){

				/* $('.category-left-box').each(function(){
				 parent_height($(this), $(this).parent('.category-left'));
				 }) */

        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        //fixed menu
				if($('.t-header').length){
          var element_top = $('.t-header').offset().top;
          if(scrolled >= element_top){
            $('.t-header-in').addClass('fixed-menu');
          }
          else{
            $('.t-header-in').removeClass('fixed-menu');
          }
				}

        //fixed menu END
        var scrollTop = $(window).scrollTop();
        var windowHeight = $(window).height();

        //menu active
        $('[data-category]').each(function(){
					/* var offset = $(this).offset().top;
					 if(scrollTop + windowHeight > offset){
					 var n = $(this).data('category');
					 n = n.toString();

					 $('[data-menu]').each(function(){
					 var m = $(this).data('menu');
					 m = m.toString();
					 console.log(n);
					 if(n == m){
					 $(this).addClass('active');
					 }
					 })
					 } */

        })
        //menu active END

        //fixed left filter
        function fixed_filter(){
          $('.category-in').each(function(){
            var cat_list_top = $(this).offset().top;
            var cat_list_bottom = $(this).offset().top + $(this).outerHeight();
            cat_list_bottom = cat_list_bottom - $(this).find('.category-left-box').outerHeight() - $('.t-header-in.fixed-menu').height();
            var scroll_stop = 20;
            if(scrolled >= cat_list_top - $('.t-header-in.fixed-menu').height() - scroll_stop){
              if(scrolled <= cat_list_bottom - (scroll_stop * 2)){
                $(this).find('.category-left-box').css('top', scrolled - cat_list_top + $('.t-header-in.fixed-menu').height() + scroll_stop);
              }
              else{

              }
            }
            else{
              $(this).find('.category-left-box').css('top', 0);
            }
          })
        }

        //fixed left filter END





        //cart
        if($('.cart-row').length){
          var offset = $('.cart-row').offset().top;
        }

        if(!($('.cart-row').hasClass('in-window'))){
          if(scrollTop + windowHeight - (windowHeight/3) > offset){
            $('.cart-overlay').fadeIn();
            var scr = (scrollTop + windowHeight) - offset - (windowHeight/5);
            if(!($('.cart-row').hasClass('utte'))){

              $('.cart-row').css('margin-top', -scr);
              $('.cart-row').addClass('utte');
              $('.categories-row').addClass('scaled');
            }
          }
          else{
            fixed_filter();
            if(!$('body').hasClass('product-open')){
              $('.categories-row').removeClass('scaled');
            }

            $('.cart-row').css('margin-top', '0');
            $('.cart-row').removeClass('utte');
            $('.cart-overlay').fadeOut();
          }
        }


        //cart END

				//cart success fixed
				if($('.js-cart-row-success').length && $(window).width() > 767){
					parent_height($('.js-cart-success-in'), $('.js-cart-row-success'));
					if($(window).scrollTop() + $(window).height() < $('.js-cart-row-success').offset().top){
						$('.js-cart-success-in')
							.css({
								'position' : 'fixed',
								'left' : '0',
								'right': '0',
								'top' : $(window).height() - 80,
								'z-index': '110'
							})
					}
					else{
						$('.js-cart-success-in')
							.css({
								'position': '',
								'left' : '',
								'right' : '',
								'top' : '',
								'z-index': ''
							})
					}
				}
				//cart success fixed END

      }).scroll();

      //call cart click
      function showBasket() {
        "use strict";

        $(this).addClass('active');
        $('.cart-row-out')
          .css({
                 'top' : 0,
                 'padding-top' : 40
               })
          .animate({
                     'top' : -($('.cart-row').offset().top - $(window).scrollTop())
                   }, 600, function(){
            $(this)
              .addClass('in-window')
              .addClass('fixed-cart')
          })
        $('.cart-overlay').fadeIn(700);
        $('.categories-row').addClass('scaled');

        $('.cart-close').show();

        $('body').addClass('modal-open');
      }
      $(document).on('click', '[data-need="basket"]', function() {
        "use strict";

        if ($('.cart-row').length <= 0) {
          sendAjax(this, { MODULE: 'BASKET', ACTION: 'GET' })
            .then(function(data) {
              $('footer').before(data);
              showBasket();

              setTimeout(function(){
                tabFunction();
                kladrBasket();
                minPrice();
                selectTimePizza();
    
              }, 200);
            })
          ;
        } else {
          showBasket();
        }
      })
      //call cart click END

      //close cart
      $(document).on('click', '[data-close="basket"]', function(){

        $('.cart-row-out')
          .css({
            'top' : -($('.cart-row').offset().top - $(window).scrollTop())
          })
          .animate({
            'top' : 0
          }, 600, function(){
            $(this)
              .removeClass('in-window')
              .removeClass('fixed-cart');
            $(this).css({'padding-top' : ''})
            $('.cart-row').css('height', '');
            $('.categories-row').removeClass('scaled');
          })
        ;
        $('.cart-overlay').fadeOut(700);

        $('.cart-close').hide();
        $('body').removeClass('modal-open');
        $('[data-need="basket"]').removeClass('active');
      })
      //close cart END

			/* $(document).on('mousewheel DOMMouseScroll', '.cart-row.utte, .cart-overlay, footer', function(e){
			 var e0 = e.originalEvent,
			 delta = e0.wheelDelta || -e0.detail;
			 this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
			 e.preventDefault();


			 $('.cart-row').css('margin-top', $(window).scrollTop() - $('.cart-row').offset().top);


			 }) */



      $(document).on('click', '.t-header-in nav>ul>li>a', function(){

        var go_cat = $(this).data('menu');

        if($('[data-category=' + go_cat + ']').length > 0){
          $('.meal-menu nav>ul>li>a').parent('li').removeClass('active');
          $(this).parent('li').addClass('active');
          $('html, body').animate({
            scrollTop: $('[data-category=' + go_cat + ']').offset().top - $('.t-header-in').height()
          }, 500)
        }
        else{
          console.log('такой категории еще нет');
        }

      })
      $(document).on('mouseenter click', '.cart-extra-item', function(){
        $(this).find('.cart-sl-title').slideDown(200);
        $(this).find('.cart-sl-count').slideDown(200);
      })
      $(document).on('mouseleave', '.cart-extra-item', function(){
        $(this).find('.cart-sl-title').slideUp(200);
        $(this).find('.cart-sl-count').slideUp(200);
      })


    }
    //width more than 768 END
    else{

			$(document).on('click', '.mobile-left-menu-in nav>ul>li>a', function(){
				var go_cat = $(this).data('menu');

        $('.cart-close').hide();
        $('body').removeClass('modal-open');
        $('[data-need="basket"]').removeClass('active');

				if($('[data-category=' + go_cat + ']').length > 0){
					if($('.mobile-menu-btn').hasClass('active')){
						$('.mobile-menu-btn').click();
					}
					$('.meal-menu nav>ul>li>a').parent('li').removeClass('active');
					$(this).parent('li').addClass('active');
					$('html, body')
						.animate({
							scrollTop: $('[data-category=' + go_cat + ']').offset().top - $('.t-header-in').height()
						}, 500)
				}
				else{
					alert('такой категории еще нет');
				}

       
			})

      //fixedMenu mobile
      var lastScrollTop = 0;
      $(window).scroll(function(event){
        var st = $(this).scrollTop();

        if(st > $('.f-mobile-top').height() + 100){
          $('.f-mobile-top').addClass('fixed');
          if (st > lastScrollTop){//downscroll

            $('.f-mobile-top').removeClass('show');
          }
          else { // upscroll
            $('.f-mobile-top').addClass('show');
          }
          lastScrollTop = st;
        }
        else{
          $('.f-mobile-top').removeClass('fixed');
        }


      });

      //fixedMenu mobile END




    }

  }).resize();


  $(document).on('click', '.empty-cart-menu>ul>li>a', function(){

    $('.cart-row-out')
          .css({
            'top' : -($('.cart-row').offset().top - $(window).scrollTop())
          })
          .animate({
            'top' : 0
          }, 600, function(){
            $('.cart-row-out')
              .removeClass('in-window')
              .removeClass('fixed-cart');
            $('.cart-row-out').css({'padding-top' : ''})
            $('.cart-row').css('height', '');
            $('.categories-row').removeClass('scaled');
          })
        ;
        $('.cart-overlay').fadeOut(700);

        $('.cart-close').hide();

    $('body').removeClass('modal-open');
    $('.cart-row-out').removeClass('active');

    var go_cat = $(this).data('menu');

    if(go_cat == 6){
      
      $('html, body').animate({
          scrollTop: $('[data-category=' + go_cat + ']').offset().top - 200 - $('.t-header-in').height()
      }, 500);

      $(".cart-row.utte").css('margin-top', '0px');
    }
    else{

      if($('[data-category=' + go_cat + ']').length > 0){
      $('html, body').animate({
          scrollTop: $('[data-category=' + go_cat + ']').offset().top - $('.t-header-in').height()
      }, 500);

      }
      else{
        alert('такой категории еще нет');
      }

    }

      })

  //click add to cart
  function setQuantity(that, quantity, refresh) {
    'use strict';
    
    let $t = $(that).closest('.product-modal-content').find('.ingreds-row form');

      let dataIngridients = $t.serializeJSON();

    

    sendAjax(that, {
      id: $(that).closest('.product-wrapper').data('productid'),
      basketID: $(that).closest('.product-wrapper').data('id'),
      getcartline: refresh ? 1 : 0,
      quantity: quantity,
      ingridients: dataIngridients,
      ACTION: 'CHANGE',
      MODULE: 'BASKET'
    })
      .then(function(data) {
        
        let $t = $('[data-productid="' + data.id + '"]');

        // $t.find('.count-t').text(data.newCount);

        $('.m-basket .m-basket-price').text(data.sum);
        $('.m-basket .m-basket-count').text(data.count);
        $('.cart-product-total .prod-price').text(data.sum);
        $('.cart-checkout .cart-total-summ span').text((parseInt(data.sum.replace(/\D/g, '')) + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $t.filter('.cart-product-one').find('.prod-price').text(data.totalPrice);

        let $buttons = $t.find('.cat-product-count');
        if ($buttons.length > 0) {
          if (data.newCount > 0) {
         //   $buttons.addClass('active').siblings().removeClass('active');
               $buttons.siblings().animate({opacity: '0.5'}).text('Добавлено').delay(100).animate({opacity: '1'});
              $buttons.siblings().addClass('button_active');
              setTimeout(function(){
                $buttons.siblings().animate({opacity: '0.5'}).text('В корзину').delay(100).animate({opacity: '1'});
                $buttons.siblings().removeClass('button_active');
              }, 2000);
              
          } else {
            //$t.filter('.cart-product-one').remove();
            $buttons.removeClass('active').siblings().addClass('active');
          }
        }
            sendAjax(this, { MODULE: 'BASKET', ACTION: 'GET' })
              .then(function(data) {
                $('.cart-row .cart-left').replaceWith($(data).find('.cart-left'));
              })
            ;
        // if ($('.cart-product-row').length <= 0) {
        //   if ($('.cart-row').length > 0) {
        //     sendAjax(this, { MODULE: 'BASKET', ACTION: 'GET' })
        //       .then(function(data) {
        //         $('.cart-row .cart-left').replaceWith($(data).find('.cart-left'));
        //       })
        //     ;
        //   }
        // } else {
        //   if (data.cartline != '') {
        //     $('.cart-product-row').append(data.cartline);
        //   }
        // }
        orderSumAgree();
          // $('.ingreds-row form').get(0).reset();
          // var price = 0;
          // var finalPrice = parseInt($('.final-price').data('base'));
          // $('.final-price').text(finalPrice);
      });
    return false;
  }
  function deleteProduct(that) {
    sendAjax(that, {
      id: $(that).closest('.product-wrapper').data('productid'),
      basketID: $(that).closest('.product-wrapper').data('id'),
      ACTION: 'REMOVE',
      MODULE: 'BASKET'
    })
      .then(function(data) {
        let $t = $('[data-productid="' + data.id + '"]');
        $t.filter('.cart-product-one').remove();

        $t.find('.count-t').text(0);
        $('.m-basket .m-basket-price').text(data.sum);
        $('.m-basket .m-basket-count').text(data.count);
        $('.cart-product-total .prod-price').text(data.sum);
        $('.cart-checkout .cart-total-summ').text(data.sum);

        let $buttons = $t.find('.cat-product-count');
        if ($buttons.length > 0) {
          $buttons.removeClass('active').siblings().addClass('active');
        }

        if ($('.cart-product-one').length <= 0) {
          if ($('.cart-row').length > 0) {
            sendAjax(this, { MODULE: 'BASKET', ACTION: 'GET' })
              .then(function(data) {
                $('.cart-row .cart-left').replaceWith($(data).find('.cart-left'));
              })
            ;
          }
        }
      });
    return false;
  }
  $(document)
    .on('click', '.cat-product-bas', function(e) { return setQuantity(this, 1, true); })
    .on('click', '.count-minus', function(e) { return setQuantity(this, -1); })
    .on('click', '.count-plus', function(e) { return setQuantity(this, +1, true); })
    .on('click', '.cart-product-delete', function(e) { return deleteProduct(this); })
  ;
  //click add to cart END

  //product modal
  function showProductModal($t) {
    $('body')
      .addClass('modal-open product-open')
      .append('<div class="product-backdrop"></div>');
    $('.categories-row').addClass('scaled');
    $t.css({'display': 'block', 'top': '100%'});

    let sto_top = 0;
    if ($(window).width() >= 768) {
      sto_top = $(window).height() - $t.find('.product-modal-in').height();
      sto_top = sto_top > 20 ? sto_top / 2 : 20;
    }
    $t.animate({'top': sto_top}, 400);
    $('.product-backdrop').fadeIn();
    /* history.pushState({}, 'cart', '/cart/' ); */
    location.hash = '#id' + $t.data('productid');

  }
  function hideProductModal($t) {
    $t.animate({ 'top' : '100%' }, function() {
      $(this).css({ 'display' : 'none' });
    });
    $('.categories-row').removeClass('scaled');
    $('.product-backdrop').fadeOut(function() {
      $(this).remove();
    });
    $('body').removeClass('modal-open product-open');
    /* history.back(); */
    //history.pushState('',document.title,window.location.pathname);
    location.hash = '#none';
  }
  $(document)
    .on('click', '[data-btn="open-product"]', function() {
      let id = $(this).closest('.product-wrapper').data('productid'),
        $t = $('.product-modal[data-productid="' + id + '"]');
      if ($t.length > 0) {
        showProductModal($t);
      } else {
        sendAjax(this, {id: id, ACTION: 'DETAIL', MODULE: 'CATALOG'})
          .then(data => {
            let $t = $(data);
            $('body').append($t);
            showProductModal($t);
          });
      }
      return false;
    })
    .on('click', '[data-btn="product-close"]', function() {
      hideProductModal($(this).closest('.product-modal'));
      return false;
    })
    .on('click', '.product-backdrop',function() {
      hideProductModal($('.product-modal:visible'));
      return false;
    })
  ;
  //product modal END


  //navbar xs

  $(document).on('click', '[data-toggle="collase"]', function(){
    var target = $(this).data('target');
    $(target)
      .slideToggle(function(){
      })
      .toggleClass('active');
    $(this).toggleClass('active');
  })

  //navbar xs END


  //личный кабинет клик по заказу
  function toggleHistory(hisparent) {
    "use strict";

    if(hisparent.hasClass('active')){
      hisparent.removeClass('active');
      hisparent.find('.history-detail').slideUp(200);
    }
    else {
      $('.history-one').removeClass('active');

      $('.history-one').each(function(){

        $(this).find('.history-detail').slideUp(200);

      });

      hisparent.addClass('active');
      hisparent.find('.history-detail').slideDown(200);
    }
  }

  $(document).ready(function(){
    $(document).on('click', '.history-visible-info', function(event){
      var hisparent = $(this).parents('.history-one');
      if(hisparent.find('.history-detail').length <= 0){
        sendAjax(hisparent[0], { MODULE: 'BASKET', ACTION: 'GETORDER', ID: hisparent.data('id') })
          .then(function(data) {
              if(hisparent.find('.history-detail').length <= 0){
                $(this).append(data);
                toggleHistory(hisparent);
                MyRating();
              }
          });
      }
      else{
        toggleHistory(hisparent);
      }

    });
  });

  


  //личный кабинет клик по заказу END



  //клик меню кафе
  if ($('.cafe-menu-title').length > 0) {
    $(document).on('click', '.cafe-menu-title', function() {
      var cmenu = $(this).data('cmenu').toString();

      $('[data-cmenu]').removeClass('active');
      $(this).addClass('active');

      $('[data-tmenu]')
        .fadeOut(100, function() {
          $(this).removeClass('active');
        });
      $('[data-tmenu="' + cmenu + '"]')
        .fadeIn(400, function() {
          $(this).addClass('active');
        });
    });
    $('.cafe-menu-title').first().click();
  }
  //клик меню кафе END


  //tabs ТРЕБУЕТ ИЗМЕНЕНИЙ! РАБОТАЕТ, НО ТУТ БУДУТ ИЗМЕНЕНИЯ!


  function tab(tab_parent, tab_btn_eq) {



    var	tab_btn_row = tab_parent.find('.tab-btn-row'),
      tab_content_row = tab_parent.find('.tab-content-row');


    if(typeof tab_parent.data('mode') != 'undefined' && tab_parent.data('mode') == 'toggle'){
      tab_content_row.find('.tab-content-one').hide();

    }
    else{

      tab_btn_row.find('.tab-btn-one').removeClass('active');
      tab_btn_row.find('[data-tabbtn="' + tab_btn_eq + '"]').addClass('active');

      tab_content_row.find('.tab-content-one').hide();
      tab_content_row.find('[data-tabcontent="' + tab_btn_eq + '"]').show();
    }

  }



  function tabFunction(){
    if($('.tab-box').length){
    $('.tab-box').each(function(){
      var tab_btn_one = $(this).find('.tab-btn-one'),
        tab_content_one = $(this).find('.tab-content-one'),
        tab_btn_eq = 1;
      var i = 0;
      tab_btn_one.each(function(){

        i++;

        $(this).attr('data-tabbtn', i);

        if($(this).hasClass('active')){
          tab_btn_eq = $(this).data('tabbtn');
        }
      })

      var n = 0;
      tab_content_one.each(function(){
        n++;
        $(this).attr('data-tabcontent', n);
      })
      tab($(this), tab_btn_eq);
    })
  }
  }

  tabFunction();

  $(document).on('click', '.tab-btn-one', function(){
    var tab_parent = $(this).parents('.tab-box');

    if(tab_parent.data('mode') == 'toggle'){
      var clicked_elem = $(this);

      tab_parent.find('.tab-content-one').hide();

      if(!$(this).hasClass('active')){
        $(this).addClass('active');
        tab_parent.find('.tab-btn-one').not($(this)).removeClass('active');
        tab_parent.find('[data-tabcontent="' + $(this).data('tabbtn') + '"]').show();
      }
      else{
        $(this).removeClass('active');

        tab_parent.find('[data-tabcontent="' + $(this).data('tabbtn') + '"]').hide();
      }
    }
    else{
      tab(tab_parent, $(this).data('tabbtn'), $());
    }
  })
  //tabs END

  //roll-up
  if($('[data-btn="roll-up-btn"]').length){
    function rolled(){
      $('[data-btn="roll-up-btn"]').each(function(){
        var rollParent = $(this).parents('.roll-up-parent');
        if(rollParent.hasClass('rolled')){
          var r  = rollParent.height() - $(this).height();
          rollParent
            .css({
              'margin-top' : -r
            })
        }
      })
    }
    rolled();
    $(window).on('resize', function(){
      rolled();
    })
  }


  $(document).on('click', '[data-btn="roll-up-btn"]', function(){
    var rollParent = $(this).parents('.roll-up-parent');
    if(rollParent.hasClass('rolled')){
      rollParent
        .removeClass('rolled')
        .animate({
          'margin-top' : '0'
        }, function(){
          $(this).css({
            'margin-top' : ''
          })
        })
    }
    else{
      var r  = rollParent.height() - $(this).height();
        $.ajax({

        })
      rollParent
        .css({
          'margin-top' : '0'
        })
        .animate({
          'margin-top' : -r
        })
      rollParent.addClass('rolled');
    }
  })

  //roll-up END

	

  //pretty select END

  //время зависит от даты
	
	function selectTimePizza(){

    //вывод даты отложенного заказа
  for(var i = 0; i < 7; i++){
    
    var d = new Date(new Date().getTime() + i * 24 * 60 * 60 * 1000);

    var day=new Array("вс","пн","вт", "ср","чт","пт","сб");

    var month=new Array("января","февраля","марта","апреля","мая","июня",
    "июля","августа","сентября","октября","ноября","декабря");
    
    
    // $('.date-del-in').append('<div class="select-one">' + d.getDate() + " "  + month[d.getMonth()] + ", " + day[d.getDay()] + '</div>');
  }
  
  
  
  //вывод даты отложенного заказа END
  var timed = new Date();
  var hours = timed.getHours();
  var minutes = timed.getMinutes();

  if(minutes > 0){
    $('.error__time_info').children('p').children('span').text((hours + 1) + ':' + minutes);
  }
  else if(hours >=23){
    $('.error__time_info').children('p').children('span').text('00' + ':' + minutes);
  }
  else{
    $('.error__time_info').children('p').children('span').text((hours + 1) + ':00');
  }


  $('.del-one.tab-btn-one').click(function(){
    if($(this).is(':last-child')){
        if($('.time-one.tab-btn-one:first-child').hasClass('active')){
          $('.error__time_info').css('display', 'block');
        }
    }
    if($(this).is(':first-child')){
      $('.error__time_info').css('display', 'none');
    }
  });


  


  //pretty select
  if($('.select-wrap').length){

    function thisDayClick(){
      $('.select-wrap').each(function(){

      var first_select = $(this).find('.select-list').find('.select-one:first');

       var firstDay_select = $(this).find('.date-del-in').find('.select-one:first'); 

       var selectedDay = $(this).find('.date-del-in').find('.select-one'); 

       firstDay_select.addClass('this_day'); 

       var delTime = $(this).find('.select-list').find('.select-one');

      var select_hidden = $(this).find('.select-hidden');
      $(this).find('.selected-box').text(first_select.text());
      first_select.addClass('selected');
      if(select_hidden.length){
        select_hidden.val(first_select.text());
      }

      var endTime = $(this).find('.date-del-in').find('.selected').attr('data-end') + '';


      var timeArr = endTime.split(':', 2);
      var endHour = timeArr[0];
      var endMinutes = timeArr[1];

       if(selectedDay.hasClass('selected')){


        if(selectedDay.hasClass('this_day')){
          setTimeout(function(){
            $(".select-list-time").children('.select-one').remove();
          }, 50);
          

          setTimeout(function(){
            if(minutes < 30){
              $(".selected-box-time").text(hours+1);
              for(var i = hours + 1; i < endHour; i++){
                  $(".select-list-time").append('<div class="select-one time-del">' + i + '</div>');
                }
              }
            else{
              $(".selected-box-time").text(hours+2);
              for(var i = hours + 2; i < 24; i++){
                  $(".select-list-time").append('<div class="select-one time-del">' + i + '</div>');
                }
            }
          }, 60);
          
       
        }  
      } 

    });
    }

    thisDayClick();
    



    $('.select-wrap').children('.date-del-in').children('.select-one').click(function(){
      if($(this).hasClass('this_day')){
        thisDayClick();
      }
      else{
        $('.js-select-time').children('.time-del').remove();
      }
      
    });







  
    $(document).on('click', '.selected-box', function(){
    
      var select_parent = $(this).parents('.select-wrap');

      var select_list = select_parent.find('.select-list');



      $('.select-wrap').not(select_parent).removeClass('active');
    select_parent.toggleClass('active');

    });

    $(document).on('click', '.selected-box1', function(){
    
      var select_parent = $(this).parents('.select-wrap');

      var select_list = select_parent.find('.select-list');

      $('.select-wrap').not(select_parent).removeClass('active');
    select_parent.toggleClass('active');

    })

    $(document).on('click', '.select-one', function(){



      var select_parent = $(this).parents('.select-wrap');

      var select_hidden = select_parent.find('.select-hidden');

      select_parent.find('.select-one').not($(this)).removeClass('selected');
      $(this).addClass('selected');
      select_parent.find('.selected-box').text($(this).text());
      select_parent.removeClass('active');

      if(select_hidden.length){
        select_hidden.val($(this).text());
      }
    })
  }

    $(document).mouseup(function (e) {
        var container = $(".select-list");
        if (container.has(e.target).length === 0 && container.parents('.select-wrap').has(e.target).length === 0){
            if(container.parents('.select-wrap').hasClass('active')){
                container.parents('.select-wrap').removeClass('active');
            }
        }
    });


    if($('.js-select-date-one').length){
    function start_time(){
      var start_time = $('.js-select-date-one.selected').data('start');
        start_time = start_time.split(':');
      return start_time;
    }
    function start_hour(){
      var start_hour = start_time()[0];
        start_hour = parseInt(start_hour);
      return start_hour;
    }
    
    function start_min(){
      var start_min = start_time()[1];
        start_min = parseInt(start_min);
      return start_min;
    }
    
    function end_time(){
      var end_time =  $('.js-select-date-one.selected').data('end');
        end_time = end_time.split(':');
      return end_time;
    }
    
    function end_hour(){
      var end_hour = end_time()[0];
        end_hour = parseInt(end_hour);
      return end_hour;
    }
    
    function end_min(){
      var end_min = end_time()[1];
        end_min = parseInt(end_min);
      return end_min;
    }
    
    /* var start_time = $('.js-select-date-one.selected').data('start');
      start_time = start_time.split(':');
    var start_hour = start_time[0],
      start_min = start_time[1];
      
      start_hour = parseInt(start_hour);
      start_min = parseInt(start_min);

    var end_time =  $('.js-select-date-one.selected').data('end');
      end_time = end_time.split(':');
      
    var end_hour = end_time[0];
      end_min = end_time[1];
      end_hour = parseInt(end_hour);
      end_min = parseInt(end_min); */
      
      
    for(var i = start_hour(); i <= end_hour(); i++){
      
      $('.js-select-time').append('<div class="select-one js-select-time-one">' + i + '</div>');
    }
    
    $('.js-select-time-one:first').addClass('selected');
    
    $('.js-selected-time').text($('.js-select-time-one:first').text());
    
    for(var i = start_min(); i <= 50; i+=10){
      var add_z = '';
      var lin_i = i.toString().length;
      if(lin_i == 1){
        add_z = 0;
      }   
      $('.js-select-min').append('<div class="select-one js-select-min-one">' + add_z + i + '</div>');
    }
    
    $('.js-select-min-one:first').addClass('selected');
    
    $('.js-selected-min').text($('.js-select-min-one:first').text());
    
    
  }
  
  $('.js-selected-time').bind("DOMSubtreeModified",function(){
    $('.js-select-min-one').remove();
    
    $('.js-selected-min').text('');
    
    
    var sel_time = $(this).text();
    sel_time = parseInt(sel_time);
    
    end_min_n = end_min();
    
    /* if(end_min_n == 0){
      end_min_n = 60;
    } */
    
    if(sel_time != start_hour() && sel_time != end_hour()){
      for(var i = 0; i <= 50; i+=10){
        var add_z = '';
        var lin_i = i.toString().length;
        if(lin_i == 1){
          add_z = 0;
        }   
        $('.js-select-min').append('<div class="select-one js-select-min-one">' + add_z + i + '</div>');
      }
      
    }
    else if(sel_time == start_hour()){
      for(var i = start_min(); i <= 50; i+=10){
        var add_z = '';
        var lin_i = i.toString().length;
        if(lin_i == 1){
          add_z = 0;
        }   
        $('.js-select-min').append('<div class="select-one js-select-min-one">' + add_z + i + '</div>');
      }
    }
    else{
      for(var i = 0; i <= end_min_n; i+=10){
        var add_z = '';
        var lin_i = i.toString().length;
        if(lin_i == 1){
          add_z = 0;
        }   
        $('.js-select-min').append('<div class="select-one js-select-min-one">' + add_z + i + '</div>');
      }
    }
    $('.js-select-min-one:first').addClass('selected');
    $('.js-selected-min').text($('.js-select-min-one:first').text());
  })
  
  $('.js-selected-date').bind("DOMSubtreeModified",function(){
    $('.js-select-time-one').remove();
    $('.js-selected-time').text('');
    
    var end_hour_n;
    
    end_hour_n = end_hour();
    
    if(end_hour() < 23){
      end_hour_n = end_hour() + 24;
    }
    
    for(var i = start_hour(); i <= end_hour_n; i++){
      
      var n = i;
      
      if(n > 23){
        n = i - 24;
      }
      $('.js-select-time').append('<div class="select-one js-select-time-one">' + n + '</div>');
    }
    
    $('.js-select-time-one:first').addClass('selected');
    $('.js-selected-time').text($('.js-select-time-one:first').text());
    
  })
  
  //время зависит от даты END
  
  
  //open one element

  if($('[data-open="one-elem"]').length){
    $('[data-open="one-elem"]').each(function(){
      var elem_target = $(this).data('target');
      $(elem_target).hide();
    })

    $(document).on('click', '[data-open="one-elem"]', function(){
      var elem_target = $(this).data('target');
      $('input[name="myaddrid"]:checked').prop('checked', false);
      $(elem_target).slideToggle();
    })
  }
  }

  selectTimePizza();

  //open one element END



	//ползунок, input баллов

	if($(".cart-use").length){
		$(".cart-use").slider({

			value: $(".cart-use").data('maxbonus')/2,
			min: 0,
			max: $(".cart-use").data('maxbonus'),
			step: 1,
			slide: function(event, ui){
				var bonus_width = ui.value * 100/$(".cart-use").data('maxbonus');
				$('.bonus_to_use').css({
					'width' : bonus_width + '%'
				})
				$('.cart-use-input').val(ui.value);
			},
			create: function(){
				$(this).append('<span class="bonus_to_use"></span>');
				bonus_width = $(".cart-use").slider("value") * 100/$(".cart-use").data('maxbonus');
				$('.bonus_to_use').css({
					'width' : bonus_width  + '%'
				})
				$('.cart-use-act').append('<input type="text" value="' + $(".cart-use").slider("value") + '" class="cart-use-input">');
			}

		});
	}

	$(document).on('keyup input click', '.cart-use-input', function () {
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		var max_bonus = $('.cart-use').data('maxbonus');
		if(this.value.length > max_bonus.toString().length){
			this.value = this.value.substring(0,3);
		}
		if(this.value > max_bonus){
			this.value = max_bonus;
		}

		$('.cart-use').slider({
			'value' : this.value
		});
		$('.bonus_to_use').css({
			'width' : this.value * 100 / max_bonus + '%'
		})

	});

	$(document).on('focusout', '.cart-use-input', function(){

		if(this.value == ''){
			this.value = 0;
		}
	})

	//ползунок, input баллов END

	//обновление страницы при изменении окна в зависимости от ширины экрана

var rtime;
	 var timeout = false;
var delta = 100;
var windowWidth1 = $(window).width();
$(window).resize(function() {
	 rtime = new Date();
	 if (timeout === false) {
	 timeout = true;
	 setTimeout(resizeend, delta);
	 }
	 });

function screen_check(window_width){
	var screen_know;

	if(window_width < smScreen){
		screen_know = 'xs';
	}
	else if(window_width >= smScreen && window_width < mdScreen){
		screen_know = 'sm';
	}
	else if(window_width >= mdScreen && window_width < lgScreen){
		screen_know = 'md';
	}
	else if(window_width >= lgScreen){
		screen_know = 'lg';
	}
	return screen_know;
}

var screen_me = screen_check($(window).width());


	 function resizeend() {
	 if (new Date() - rtime < delta) {
	 setTimeout(resizeend, delta);
	 } else {
	 timeout = false;
		if(windowWidth1 != $(window).width() && screen_me != screen_check($(window).width())){
	 location.reload();
	 }
	 }
}



  //обновление страницы при изменении окна END


  //Ввод промо кода

  $(document).on('click', '.js-promo-get', function(){
    var parent_form = $(this).parents('.styled-input-box'),
        form_val = parent_form.find('.js-promo-input').val();
        form_val = form_val.toLowerCase();
        $.ajax({
            url: '/local/dev/promo.php',
            dataType: "json",
            data: {'promo' : form_val},
            success: function(data){
              if(data.ID > 0){
                  parent_form.parents('.promo-box').find('.coupon-name').text(data.DISCOUNT_NAME);
                  parent_form.parents('.promo-box').addClass('show-success');
              }
              else{
                  parent_form.parents('.promo-box').addClass('show-error');
              }
            }
        });
    return false;
  })

  //Ввод промо кода END

    $(document).on('click input', '.js-promo-input', function(){
      $(this).parents('.promo-box')
          .removeClass('show-error')
          .removeClass('show-success');
    })

    $(document).mouseup(function (e) {
        var container = $(".promo-info-box");
        if (container.has(e.target).length === 0){
            if(container.parents('.promo-box').hasClass('show-error')){
                container.parents('.promo-box').removeClass('show-error');
            }
            if(container.parents('.promo-box').hasClass('show-success') ){
                container.parents('.promo-box').removeClass('show-success');
            }

        }
    });

	 $(document).on('click', '.js-promo-cancel', function(){
	    $(this).parents('.promo-box').find('.js-promo-input').val('');
         $(this).parents('.promo-box').removeClass('show-success');
     })

    $(document).on('click', '.js-promogo-order', function(){
        $(this).parents('.promo-box').find('.js-promo-input').val('');
      $(this).parents('.promo-box').removeClass('.show-success');
        $('html, body').animate({
            scrollTop: $(".category-row").offset().top
        }, 2000);

    });

  //checkbox обработка данных
  $(document).on('change', '.js-check-agree', function() {
      "use strict";
      let $t = $(this), isNotChecked = !$t.is(":checked");
      $t.closest('.js-to-checkbox').find('.js-check-agree-btn').prop('disabled', isNotChecked);
      if(isNotChecked) {
        $t.closest('.js-agree-shake').effect('shake', { distance: 5, times: 2 });
      }
      orderSumAgree();
      minPrice();
    });

  $('.category-row').each(function(){
    if($(this).children('a[name="pizza"]').length > 0){
      $(this).children('.cat-product-one').children('.cat-product-img').hide();
    }
  });



  function minPrice(){
    var sum = $('.cart-total-summ').text();
    var label = parseInt(sum.replace(/\D/g, '')),
        label1 = $('.error_cart_message');
    if(label >=290){
      label1.hide();
    }
    else if(label1 == 0){
      label1.hide();
    }
    else{
      label1.show();
    }
  }
  function orderSumAgree(){
    var sum = $('.js-check-agree-btn span').text(),
        label = $('.error_cart_message'),
        result = (parseInt((sum.replace(/\D/g, ''))) >= 290);
    if(sum == 0) return false;
    if(result) label.hide();
    else{
    	label.show();
    	$('.js-check-agree-btn').prop('disabled', true);
    }
    return result;
  }
  // $(document).ready(function(){

  //     var cartSumm = $('.cart-total-summ span');

  //     var summ = parseInt(cartSumm.replace(/\s/g, ''), 10);
  //     console.log(summ);
    
  // });

  //checkbox обработка данных END


  //copy box
  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
  }

  $(document).on('click', '.styled-copy-btn', function(){

    var copy_parent = $(this).parents('.styled-copy-box');
    copyToClipboard(copy_parent.find('.styled-copy-text'));
    copy_parent.addClass('copied');
    $(this).text('Скопировано');
  })
  //copy box END


  //ширина меню, добавление кнопки еще

  if($('.t-header').length){
    var theader_width = $('.t-header-in').width(),
      meal_width = $('.meal-menu').outerWidth(),
      stock_width = $('.stock-box').outerWidth(),
      right_width = $('.t-right-menu').outerWidth();

    var allow_width = theader_width - stock_width - right_width;

    if(allow_width - meal_width < 0){
      var totalWidth = 0;
      var will_width = 0;
      var man;
      $('.t-header-in .meal-menu').find('nav')
        .append('<div>\
							<div class="more_meal">Еще\
								<ul class="meal-more-list">\
								</ul>\
								</div>\
						</div>');
      $('.t-header-in .meal-menu').find('nav').children('ul').children('li').each(function(){
        totalWidth += $(this).outerWidth();
        if(totalWidth < allow_width){

        }
        else{
          $('.meal-more-list').append($(this));
        }
      })

      /* $('.t-header-in .meal-menu').children('nav').css({
        'width' : 10000
      });

      $('.t-header-in .meal-menu').css({
        'width' : will_width,
        'overflow' : 'hidden'
      }); */

    }
    else{

    }
  }

  //ширина меню, добавление кнопки еще

  //добавление адреса в личном кабинете


    $(document).on('click', '.js-add-address', function(){
      if(!$(this).hasClass('inactive')) {


        $('.per-addr-add').before('\
          <div class="per-addr-one js-addr" style="display: none;">\
            <form class="js-form-address">\
              <div class="per-addr-title">\
                <label class="active">\
                  <input type="radio" name="TYPE" value="Дом" style="display:none"/>\
                  <span class="inactive js-choose-add">Дом</span>\
                </label>\
                <label>\
                  <input type="radio" name="TYPE" value="Работа" style="display:none" />\
                  <span class="inactive js-choose-add">Работа</span>\
                </label>\
                <label>\
                  <input type="radio" name="TYPE" value="Другое" style="display:none" />\
                  <span class="inactive js-choose-add">Другое</span>\
                </label>\
              </div>\
              <div class="add-addr-values">\
                <div class="adrr-input">\
                  <div class="addr-row">\
                    <input type="text" style = "display: none;" class = "feed_inp inp_recr input_city input_city1" name="city" autocomplete="off" data-kladr-type="city" data-kladr-id="1800000100000" value="Ижевск">\
                    <input id="order-street" type="text" class="cart-input js-kladr-input cart-input1" placeholder="Улица" name="STREET" />\
                  </div>\
                  <div class="addr-row">\
                    <input type="text" class="cart-input house-input" placeholder="Дом" name="HOUSE" />\
                    <input type="text" class="cart-input housing-input" placeholder="Корпус" name="HOUSING" />\
                    <input type="text" class="cart-input appart-input" placeholder="Кв." name="APPART" />\
                  </div>\
                  <div class="addr-row-ot">\
                    <input type="text" class="cart-input entrance-input" placeholder="Подъезд" name="ENTRANCE" />\
                    <input type="text" class="cart-input floor-input" placeholder="Этаж" name="FLOOR" />\
                  </div>\
                </div>\
                <div class="add-addr-comment">\
                  <textarea class="add-addr-textarea" placeholder="Комментарий к адресу" name="COMMENT[][VALUE][TEXT]"></textarea>\
                </div>\
                <div class="add-btn-row">\
                  <div class="promo-true-btn white js-addr-cancel">Отменить</div>\
                  <div class="promo-true-btn red js-addr-save">Сохранить</div>\
                </div>\
              </div>\
            </form>\
          </div>');

        

        $('.per-addr-one:last').slideDown();
        $(this).addClass('inactive');
        activeLabel();
        kladrBasket();

      }
      return false;
    });


  


  function activeLabel(){
    $('.per-addr-title').on('click', 'label', function(){
    $('.per-addr-title label').removeClass('active');
    $(this).addClass('active');
    });
  }

  

  $(document).on('click', '.js-choose-add', function(){
    $(this).parent('label').siblings().removeClass('active');
    $(this).parent('label').addClass('active');
    $(this).addClass('active');
  });

  $(document).on('click', '.js-addr-cancel', function() {
    $(this).parents('.js-addr')
      .slideUp(function(){
        $(this).remove();
      })

    $('.js-add-address').removeClass('inactive');
  });

  $(document).on('click', '.per-addr-delete', function() {
    "use strict";

    let $t = $(this).closest('.per-addr-one');
    sendAjax($t[0], { MODULE: 'PROFILE', ACTION: 'DELETE_ADDR', ID: $t.data('addrid') })
      .then(function(data) {
        $(this).slideUp(function(){
          $(this).remove();
        });
      })
    ;

    return false;
  });

  $(document).on('click', '.js-addr-save', function() {
    "use strict";

    let $form = $(this).closest('form');
    sendAjax($form[0], getFormData($form.serializeArray(), { MODULE: 'PROFILE', ACTION: 'SAVE_ADDR' }))
      .then(function(data) {
        $(this).closest('.per-addr-one').replaceWith(data);

        $('.js-add-address').removeClass('inactive');
      })
      .catch(function(data) {
        let $t = $(this)
        $.each(data, function() {
          $t.find('[name="' + this + '"]').stop().css({backgroundColor: '#e54b21'}).animate({backgroundColor: ''}, 800);
        });
      })
    ;

    return false;
  });

  //добавление адреса в личном кабинете END

  //редактор адреса в личном кабинете

  $(document).on('click', '.js-edit-addr', function() {
    if(!$(this).hasClass('editing')){
      var	addr_parent = $(this).parents('.per-addr-one'),
           addr_title = addr_parent.find('.per-addr-title');

      addr_parent.find('form').addClass('js-form-address');


      var addt = addr_title.text();

      addr_parent.find('.per-addr-title').html('\
      <label>\
                <input type="radio" name="TYPE" value="Дом" style="display:none"/>\
                <span class="inactive js-choose-add">Дом</span>\
              </label>\
              <label>\
                <input type="radio" name="TYPE" value="Работа" style="display:none" />\
                <span class="inactive js-choose-add">Работа</span>\
              </label>\
              <label>\
                <input type="radio" name="TYPE" value="Другое" style="display:none" />\
                <span class="inactive js-choose-add">Другое</span>\
              </label>');
      addr_parent.find('.js-choose-add').each(function(){
        if($(this).text() == addt){
          $(this).parent('label').addClass('active');
          $(this).addClass('active').prev().prop('checked', true);
        }
      });

      var per_street = addr_parent.find('.per-addr-street').text(),
          per_house = addr_parent.find('.per-addr-house').text(),
          per_home = addr_parent.find('.per-addr-home').text(),
          per_housing = addr_parent.find('.per-addr-housing').text(),
          per_entrance = addr_parent.find('.per-addr-entrance').text(),
          per_floor = addr_parent.find('.per-addr-floor').text(),
          per_comment = addr_parent.find('.per-addr-comment').text();
          per_id = addr_parent.data('addrid');

      addr_parent.find('.per-addr-mine').html('\
        <input type="hidden" name="ID" value="' + per_id + '" />\
				<div class="add-addr-values">\
          <div class="adrr-input">\
              <div class="addr-row">\
                      <input type="text" style = "display: none;" class = "feed_inp inp_recr input_city input_city1" name="city" autocomplete="off" data-kladr-type="city" data-kladr-id="1800000100000" value="Ижевск">\
                      <input id="order-street" type="text" class="cart-input js-kladr-input cart-input1" placeholder="Улица" value="' + per_street +'" name="STREET" />\
              </div>\
              <div class="addr-row">\
                <input type="text" class="cart-input house-input" placeholder="Дом" value="' + per_house +'" name="HOUSE" />\
                <input type="text" class="cart-input housing-input" placeholder="Корпус" value="' + per_housing +'" name="HOUSING" />\
                <input type="text" class="cart-input appart-input" placeholder="Кв." value="' + per_home +'" name="APPART" />\
              </div>\
              <div class="addr-row-ot">\
                <input type="text" class="cart-input entrance-input" placeholder="Подъезд" value="' + per_entrance + '" name="ENTRANCE" />\
                <input type="text" class="cart-input floor-input" placeholder="Этаж" value="' + per_floor + '" name="FLOOR" />\
              </div>\
              \
          </div>\
					<div class="add-addr-comment">\
						<textarea class="add-addr-textarea" placeholder="Комментарий к адресу" name="COMMENT[][VALUE][TEXT]">' + per_comment + '</textarea>\
					</div>\
					<div class="add-btn-row">\
						<div class="promo-true-btn red js-addr-save">Сохранить</div>\
					</div>\
        </div>\
			');

      addr_parent.find('.per-addr-comment').remove();
      $(this).addClass('editing');
      kladrBasket();
    }

    return false;
  })



  //редактор адреса в личном кабинете END

  //Вызов окна оценить заказ

  $(document).on('click', '.js-show-ma', function(){
    var parent_his = $(this).parents('.history-btn-row');
    parent_his.toggleClass('visible-box');
  })

  //Вызов окна оценить заказ END


  //закрытие окна Оценить заказ
  $(document).mouseup(function (e) {
    var container = $(".history-btn-row");
    if (container.has(e.target).length === 0){
      container.removeClass('visible-box');
    }
  });
  //закрытие окна Оценить заказ END

  $(document).on('click', '.js-has-account', function(){
    var phone_val = $('.order-phone').val();
    phone_val = phone_val.replace(/[^0-9]/g, '');

    if(phone_val == '79999999999'){

      var targ = $('#has-account-box');
      var sto_top = $(window).height() - $(targ).find('.modal-center-in').height()
      sto_top = sto_top/2;
      if(sto_top < 20){
        sto_top = 20;
      }

      $(targ)
        .addClass('active')
        .css({
          'top' : sto_top,
          'visibility' : 'visible',
          'opacity' : '0'
        })
        .animate({
          'opacity' : '1'
        }, 300, function(){


        })

      $('body')
        .css({
          'padding-right' : $.scrollbarWidth()
        })
        .append('<div class="modal-opacity"></div>')
        .addClass('modal-open modal-open-center');
      $('.modal-opacity').fadeIn(300);

      return false;
    }

  })


  $(document).on('click', '.js-eq-btn', function(){
    var total_summ = $('.cart-total-summ').text().replace(/\D/g, '');
    total_summ = parseInt(total_summ);
    if(total_summ > 0){
      $('.oddmon-input').val(total_summ);
	  $('#sur-hid-in').prop("checked", true);
	  $('.sur-mon').removeClass('active');
    }
  })

  $(document).on('click','.js-help-oddmon', function(){
    $('.js-help-oddmon').removeClass('active');
    $(this).addClass('active');
    $('.oddmon-input').val(parseInt($(this).text()));
	$('#sur-hid-in').prop('checked', false);
  })

    $(document).on('click', '.js-ingreds', function(){
      var ingred = $(this).data('prid');
      $('#ingreds-' + ingred).toggleClass('active');
      $(this).toggleClass('active');
    })
    
	/*$(document).on('click', '.js-compose', function(){
		var product_id = $(this).parents('[data-productid]').data('productid');
		$('.cat-product-one[data-productid="' + product_id + '"]').find('[data-btn="open-product"]').trigger('click');
		console.log(product_id);
	})*/
	
	var or_money = function(total_sum){
		var sumt;
		total_sum = total_sum.replace(/[^0-9]/g, '');
		if(typeof(total_sum) == 'string'){
			sumt = total_sum.replace(/\s+/, "");
		}
		else{
			sumt = total_sum;
		}
		var arr = [ Math.ceil(sumt/100)*100, Math.ceil(sumt/500)*500, Math.ceil(sumt/1000)*1000, Math.ceil(sumt/5000)*5000  ];
		var arr2 = [];
		var arr3 = [];
		
		$.each(arr, function(i, el){
		  if($.inArray(el, arr2) === -1)
			arr2.push(el);
		});
		
		$.each(arr2, function(i, el){
		  if(el != sumt)
			arr3.push(el);
		});
		
		var s = '';
		
		arr3.forEach(function(element, index, array){
			s = s + '<span class="sur-mon js-help-oddmon">' + element + '</span>';
			if (index < arr3.length -1){
				s = s + " / ";
			}
		});
		
		s.trim();
		
		$('.sur-text-box').html(s);
	}
	
	if($('.cart-total-summ').length){
		or_money($('.cart-total-summ').html());
	}
	
	if($('.sur-mon').length){
		$('.oddmon-input').val($('.sur-mon:first-child').text());
		$('.sur-mon:first-child').addClass('active');
	}
	
	$(document).on('input', '.oddmon-input', function(){
		var total_sum = $('.cart-total-summ').html();
		var in_val = this.value;
		total_sum = total_sum.replace(/[^0-9]/g, '');
		if(total_sum != in_val){
			$('#sur-hid-in').prop('checked', false);
		}
		else{
			$('#sur-hid-in').prop('checked', true);
		}
		
		
		$('.sur-mon').each(function(){
			if($(this).text() != in_val){
				$(this).removeClass('active');
			}
			else{
				$(this).addClass('active');
			}
		})
	})
	
	$('.cart-total-summ').bind("DOMSubtreeModified",function(){
		or_money($(this).html());
		$('.sur-mon:first-child').addClass('active');
		$('.oddmon-input').val($('.sur-mon:first-child').text());
		$('#sur-hid-in').prop('checked', false);
	});

	$(document).on('click', '.js-product-compose', function(){
		var product_id = $(this).parents('[data-id]').data('id');
		$('#cart-ingreds-' + product_id).toggleClass('active');
	})
	
	$(document).mouseup(function (e) {
		var container = $('.cart-product-info .ingreds-row');
		if (container.has(e.target).length === 0){
			container.removeClass('active');
		}
	});

	
	
	
	
	
	
  $(document).on('submit', '#login form, #logout, .loginform', function(e) {
    'use strict';
    e.preventDefault();

    sendAjax(this, getFormData($(this).serializeArray()))
      .then(function(data) {
        location.href = data;
      })
      .catch(function(data) { alert(data); })
    ;
  });

  // $(document).on('click', '.product-modal.red .cat-product-bas',function(){
  //   $('.ingreds-row form').get(0).reset();
  //   var price = 0;
  //   var finalPrice = parseInt($('.final-price').data('base'));
  //   $('.final-price').text(finalPrice);
  // });


  /* change personal data */
  function changeUserData() {
    "use strict";

    let $t = $(this), name = $t.attr('name'), value = $t.val();
    value = name != "UF_SUBSCRIBE" || value == 1 ? value : 0;
    if (name == "EMAIL" && value == $('[name="first_email"]').val()) {
      value = '';
    }
    sendAjax(this, { ACTION: 'CHANGE', MODULE: 'PROFILE', name: name, value: value })
      .then(function(data) {
        let $t = $(this), name = $t.attr('name'), value = $t.val();
        switch (name) {
          case "PERSONAL_BIRTHDAY":
            $t.closest('per-birth').html(value);
            break;
          case "UF_SUBSCRIBE":
            $t = $t.closest('label');
          default:
            $t.stop().css({backgroundColor: '#00b4ae'}).animate({backgroundColor: ''}, 800);
            break;
        };
      })
      .catch(function(data) {
        let $t = $(this), name = $t.attr('name'), value = $t.val();
        switch (name) {
          case "PERSONAL_BIRTHDAY":
            break;
          case "UF_SUBSCRIBE":
            $t = $t.closest('label');
          default:
            $t.stop().css({backgroundColor: '#e54b21'}).animate({backgroundColor: ''}, 800);
            break;
        };
      })
    ;
  }
  $(document)
    .on('blur', '.personal-input', changeUserData)
    .on('change', '[name="UF_SUBSCRIBE"], [name="PERSONAL_BIRTHDAY"]', changeUserData)
  ;

  /* catalog filter */
  $(document).on('click', '.category-filter-list a', function() {
    "use strict";

    sendAjax(this, {
      MODULE: 'catalog',
      ACTION: 'FILTER',
      section: $(this).data('catid')
    })
      .then(function(data) {
        let $t = $(this);
        $t.parent().addClass('active').siblings().removeClass('active');

        let $wrp = $t.closest('.category-row');
        let $list = $wrp.find('.cat-product-one').hide();
        $.each(data.ids, function(i, val) {
          $list.filter('[data-productid="' + val + '"]').show();
        });
      })
    ;
    return false;
  });

  $(document).on('click', '.cart-total-btn', function() {
    "use strict";
    let $t = $(this).closest('form');
    $t.find('.error').removeClass('error');
    

    sendAjax($t[0], getFormData($t.serializeArray(), { MODULE: 'BASKET', ACTION: 'ORDER' }))
      .then(function(data) {
        
        if(data.flag){
          location.href = data.content;
        } 
        else {
    			$('[data-close="basket"]').trigger('click');
    			$('.cart-row').replaceWith(data.content);
    		}
      })
      .catch(function(data, code) {
        if (code == 4) {
          $.each(data, function(i, val) {
            $('[name="' + val + '"', this).addClass('error');
          });
        } else {
          alert(data);
        }
      })
    ;

    return false;
  });

  // show more addr
  $(document).on('click', '.cart-show-addr-link a', function() {
    'use strict';

    let $t = $(this).closest('.cart-right-box'),
        type = $t.closest('.tab-content-one').index();
    sendAjax($t[0], { MODULE: 'BASKET', ACTION: 'GETADDR', type: type })
      .then(function(data) {
        $(this).html(data);
      })
    ;

    return false;
  });


  $(document).on('submit', '#feedback_form, #callback_form, #callback_form2', function() {
    "use strict";
    sendAjax(this, getFormData($(this).serializeArray()))
      .then(function(data) {
        $(this).trigger('reset');
        $('.modal-opacity').trigger('click');
      })
    ;
    return false;
  });

  function getProduct() {
    "use strict";

    let hash = location.hash;

    let hasId = hash.substr(0, 3);
    let id = parseInt(hash.substr(3));
    if (hasId == '#id' && Number.isInteger(id)) {
      let $t = $('.product-modal[data-productid="' + id + '"]');
      if ($t.length > 0) {
        showProductModal($t);
      } else {
        sendAjax(this, {id: id, ACTION: 'DETAIL', MODULE: 'CATALOG'})
          .then(data => {
            let $t = $(data);
            $('body').append($t);
            showProductModal($t);
          });
      }

    }

    let hash1 = location.hash;

    let hasId1 = hash1.substr(0, 6);
    let id1 = parseInt(hash1.substr(6));
    if (hasId1 == '#share' && Number.isInteger(id1)) {
      let $t = $('.modal-center[data-shareid="' + id1 + '"]');
      if ($t.length > 0) {
        showModalCenter($t);
      } else {
        sendAjax(this, {id: id1, ACTION: 'DETAIL', MODULE: 'SHARES'})
          .then(data => {
            let $t = $(data);
            $('body').append($t);
            showModalCenter($t);
          });
      }
    }
  }


  getProduct();

  $(window).on('hashchange', getProduct);

  $(document).on('click', '.js-comment-btn', function() {
    "use strict";

    let $t = $(this).closest('form');
    sendAjax($t[0], getFormData($t.serializeArray()))
      .then(function(data) {
        $(this).html(data);
      })
    ;

    return false;
  });

  $(document).on('click', '.mark-order-btn-send', function(){
    var rating = $('.order-raty').html();
    $(this).parent('.mark-order-box').siblings();
    $('.mark-order-btn').parent('').addClass('order-raty-active no__before').html(rating);
    $('.mark-order-cap').css('display', 'none');
    $('.mark-order-box:after').css('right', '55px');
  });



    $(document).on('click', '.js-repeat_btn', function() {
    "use strict";

      $(this).animate({opacity: '0.5'}).text('Добавлено').delay(100).animate({opacity: '1'});
      setTimeout($.proxy(function(){
        $(this).animate({opacity: '0.5'}).text('Повторить заказ').delay(100).animate({opacity: '1'});
      }, this),  2000);

      sendAjax(this, {ID: $(this).data('order'), ACTION: 'REPEAT', MODULE: 'BASKET'})
        .then(function(data) {
          });

      return false;
    });



  $(document).on('change', '[name^="ingredients"]', function() {
    "use strict";
    var price = 0,
        finalPrice = parseInt($('.final-price').data('base'));
    $('[name^="ingredients"]:checked').each(function() {
      price = price + parseInt($(this).data('price'));
    });
    $('.final-price').text(finalPrice + price);
    
    // sendAjax($t[0], data)
    //   .then(function(data) {
    //     $(this).closest('.product-modal-content').find('.product-modal-price')
    //       .replaceWith($(data).find('.product-modal-price'));
    //     setQuantity($('.cat-product-bas'), 0, true);
    //   })
    // ;
  });


  $(document)
    .on('click', '.forgotpass', function() {
      "use strict";
      let $t = $(this).closest('form');

      $t.hide().parent().append('<form class="forgotform">\n' +
                         '  <input type="hidden" name="MODULE" value="AUTH">' +
                         '  <input type="hidden" name="ACTION" value="FORGOT">' +
                         '  <div class="cart-one-box">\n' +
                         '    <div class="cart-help-text">На указаный номер телефона будет выслан новый пароль</div>\n' +
                         '  </div>\n' +
                         '  <div class="cart-left-box cart-left-autor">\n' +
                         '    <span class="cart-left-title">Телефон</span>\n' +
                         '  </div>\n' +
                         '  <div class="cart-right-box">\n' +
                         '    <input type="tel" name="phone" class="cart-input js-phone-mask" placeholder="+7 (___) ___ __ __">\n' +
                         '  </div>\n' +
                         '  <div class="cart-left-box">\n' +
                         '  </div>\n' +
                         '  <div class="cart-right-box">\n' +
                         '    <div class="cart-bo-btns">\n' +
                         '      <div class="cart-extra-one">\n' +
                         '        <input type="submit" value="Получить пароль" class="cart-bo-btn">\n' +
                         '      </div>\n' +
                         '    </div>\n' +
                         '  </div>\n' +
                         '</form>');

      $('.forgotform [name="phone"]')
        .val($t.find('[name="phone"]').val())
        .mask('?+7 (999) 999-9999', { placeholder: '_' });

      return false;
    })
    .on('submit', '.forgotform', function() {
      let $t = $(this), phone = $t.find('[name="phone"]');
      if (phone.mask().length == 10) {
        sendAjax($t[0], getFormData($t.serializeArray()))
          .then(function(data) {
            $(this).parent().find('.loginform').show();
            $(this).remove();
          })
        ;
      } else {
        phone.stop().css({backgroundColor: '#e54b21'}).animate({backgroundColor: ''}, 800);
      }
      return false;
    })
  ;
});

 function minPrice(){
    var sum = $('.cart-total-summ').text();
    var label = parseInt(sum.replace(/\D/g, '')),
        label1 = $('.error_cart_message');
    if(label >=290){
      label1.hide();
    }
    else if(label1 == 0){
      label1.hide();
    }
    else{
      label1.show();
    }
  }

function sendAjax(target, data) {
  'use strict';
  let jqXHR = $.ajax({
    type     : 'POST',
    dataType : 'JSON',
    data     : $.extend({
      ACTION  : 'AUTH',
      MODULE  : 'AUTH',
      backurl : window.location.href
    }, data),
    url      : '/local/templates/.default/ajax/ajax.php'
  });
   minPrice();
  return jqXHR.then(data => {
    let defer = $.Deferred();
    if (!data.code) {
      defer.resolveWith(target, [data.body]);

    } else {
      defer.rejectWith(target, [data.body, data.code]);
    }
    return defer;
  });
};
function getFormData(arr, data) {
  'use strict';

  data = data || {};
  $.each(arr, function() {
      data[this.name] = this.value;
  });

  return data;
};

minPrice();

function kladrBasket(){
    $(function () {
    function setLabel($input, text) {
      text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
      $input.parent().parent().find('.feed_label').text(text + '*');
    }

    if($('.js-kladr-input').length) {
      var $street = $('.js-kladr-input');
      var $city = $('.input_city');

      $.kladr.setDefault({
        parentInput: '.js-form-address',
        verify: true,
        select: function (obj) {
          setLabel($(this), obj.type);
        },
        check: function (obj) {
          if (obj) {
            setLabel($(this), obj.type);
          }
        },
        checkBefore: function () {
          if (!$.trim($(this).val())) {
            return false;
          }
        }
      });

      $city.kladr('type', $.kladr.type.city);
      $street.kladr('type', $.kladr.type.street);
    }
  });
  }


  kladrBasket();






function formValidate(){
  $('.js-form-address').submit(function () {
                                        console.log('submit');
                                        return false;
                                    }).validate({
                                        rules : {
                                            "STREET" : {required : true, minlength: 10},
                                            "HOUSE" : {required : true, minlength: 10},
                                            "name" : {required : true, minlength: 10},
                                            "phone" : {required : true, minlength: 10},
                                            "quality" : {required : true}
                                        },
                                        messages : {
                                            "STREET" : {
                                                required : "Ошибка",
                                                minlength : "Минимальное количество символов не меннее 10"
                                            },
                                            "HOUSE" : {
                                                required : "Ошибка",
                                                minlength : "Минимальное количество символов не меннее 10"
                                            },
                                            "name" : {
                                                required : "Ошибка",
                                                minlength : "Минимальное количество символов не меннее 10"
                                            },
                                            "phone" : {
                                                required : "Не правильный номер телефона",
                                                minlength : "Минимальное количество символов не меннее 10"
                                            },
                                            "quality" : {
                                                required : "Пожалуйста примите соглашение"
                                            }

                                        },
                                        submitHandler: function(form){
                                            $.ajax({
                                                url:'/mail.php',
                                                data: $(form).serialize(),
                                                type: 'POST',
                                                cache: false,
                                                success: function(data){
                                                                        setTimeout(function() {
                                                                            // Done Functions
                                                                            th.trigger("reset");
                                                                        }, 1000);
                                                }
                                            });

                                        },
                                        errorPlacement: function (error, element) {
                                            $(element).parent().append(error.addClass('f-help f-help-error'));
                                        },
                                        errorElement : 'div'
                                    });
}

function myDate(){
  var moments = moment();
  var days = moment(moments).daysInMonth();
  var monthNumber = moment().month();
  
  var monthArray = [
  {number : '1', month : 'Январь'},
  {number : '2', month : 'Февраль'},
  {number : '3', month : 'Март'},
  {number : '4', month : 'Апрель'},
  {number : '5', month : 'Май'},
  {number : '6', month : 'Июнь'},
  {number : '7', month : 'Июль'},
  {number : '8', month : 'Август'},
  {number : '9', month : 'Сентябрь'},
  {number : '10', month : 'Октябрь'},
  {number : '11', month : 'Ноябрь'},
  {number : '12', month : 'Декабрь'}
];
  var afterArr = [];
  $(monthArray).each(function(e, i){
    if(i == monthNumber){
      afterArr[i.number] = i.month;
    }
    if(i > monthNumber){
      afterArr[i.number] = i.month;
    }
  });




 // var monthArray = {'1' : 'Январь', '2' : 'Февраль', '3' : 'Март', '4' : 'Апрель', '5' : 'Май', '6' : 'Июнь', '7' : 'Июль', '8' : 'Август', '9' : 'Сентябрь', '10' : 'Октябрь', '11' : 'Ноябрь', '12' : 'Декабрь'};
}

myDate();

formValidate();





